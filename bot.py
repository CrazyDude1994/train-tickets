# coding=utf-8
import datetime
from time import sleep
from urllib2 import URLError

from api import get_updates, send_message, get_trains


def has_tickets(data):
    for train in data["data"]["list"]:
        if len(train["types"]) > 0:
            return True

    return False


if __name__ == '__main__':

    notified, notified_return = False, False

    while True:
        now = datetime.datetime.now()
        print("[{0}] Receiving new trains...".format(now.strftime("%Y-%m-%d %H:%M")))
        try:
            trains = get_trains("2018-12-21", False)
            trains_return = get_trains("2018-12-28", True)
            if has_tickets(trains) and not notified:
                print "Tickets to Kiev"
                if send_message("Появились билеты на поезд в Киев на 21.12.2018"):
                    notified = True
                    print("Successfully notified")
            if has_tickets(trains_return) and not notified_return:
                print "Tickets to Kherson"
                if send_message("Появились билеты на обратный поезд в Херсон на 28.12.2018"):
                    notified_return = True
                    print("Successfully notified return")

            sleep(60)
        except URLError as error:
            print(error.reason)
        except Exception as e:
            print(e)
