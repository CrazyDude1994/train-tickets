import urllib2
import json

api_url = "https://api.telegram.org/bot716559421:AAHk5rjUzFQHWrSyUf-OfJTP1iKCrG-4qxc/"
api_train_url = "https://booking.uz.gov.ua/ru/train_search/"

last_id = 0

chat_id = 436025092

kherson_id = 2208530
kiev_id = 2200001


def make_request(method, params=""):
    data = urllib2.urlopen(api_url + method + params)
    return json.load(data)


def get_updates():
    return make_request("getUpdates")


def send_message(message):
    data = make_request("sendMessage", "?chat_id=436025092&text=" + message)
    return data["ok"]


def get_trains(date, reverse):
    first_id = kiev_id if reverse else kherson_id
    second_id = kherson_id if reverse else kiev_id
    data = urllib2.urlopen(api_train_url, "from={0}&to={1}&date={2}&time=00:00".format(first_id, second_id, date))
    return json.load(data)
